package model;

public class Paciente extends Usuario{

	
	private String doenca;
	private String medicamentoUtilizado;
	
	public Paciente(String nome){
		super(nome);
	}
	public String getDoenca() {
		return doenca;
	}
	
	public void setDoenca(String doenca) {
		this.doenca = doenca;
	}
	
	public String getMedicamentoUtilizado() {
		return medicamentoUtilizado;
	}
	
	public void setMedicamentoUtilizado(String medicamentoUtilizado) {
		this.medicamentoUtilizado = medicamentoUtilizado;
	}
	
	
}