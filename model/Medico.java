package model;

public class Medico extends Usuario{

	private String crm;
	private int anosExperiencia;

	public Medico (String nome){
		super(nome);
	}
	public String getCrm() {
		return crm;
	}
	
	public void setCrm(String crm) {
		this.crm = crm;
	}
	
	public int getAnosExperiencia() {
		return anosExperiencia;
	}
	
	public void setAnosExperiencia(int anosExperiencia) {
		this.anosExperiencia = anosExperiencia;
	}

	
	
}