package view;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import model.*;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.ButtonGroup;
import javax.swing.JOptionPane;
import javax.swing.JTabbedPane;
import javax.swing.JLayeredPane;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JButton;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

import javax.swing.JRadioButton;

import controller.CadastroConsulta;

import javax.swing.JList;
import javax.swing.event.AncestorListener;
import javax.swing.event.AncestorEvent;
import javax.swing.table.DefaultTableModel;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;



public class CadastroHospital extends JFrame {
	
	//private final ButtonGroup buttonGroup = new ButtonGroup();
	//private final ButtonGroup buttonGroup_1 = new ButtonGroup();
	private JPanel contentPane_Paciente;
	private JTextField textField_Nome;
	private JTextField textField_Idade;
	private JTextField textField_Peso;
	private JTextField textField_Altura;
	private JTextField textField_NumeroC;
	private JTextField textField_Medico;
	private JTextField textField_Doenca;
	private JTextField textField_Medicamento;
	private JTextField textField_Diagnostico;
	private JTextField textField_Data;
	private JTextField textField_Nome2;
	private JTextField textField_CRM;
	private JTextField textField_Anos;
	//
	private	boolean fut = false;
	private boolean tenis = false;
	Medico umFutebolista = null;
	Paciente umPaciente = null;
	ConsultaMedica umaConsulta = null;
	private JTextField textDados;

	
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					CadastroHospital frame = new CadastroHospital();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public CadastroHospital() {
		final CadastroConsulta umCadastro = new CadastroConsulta();
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 665, 511);
		contentPane_Paciente = new JPanel();
		contentPane_Paciente.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane_Paciente);
		contentPane_Paciente.setLayout(null);
		
		JLabel lblSelecioneQuemDeseja = new JLabel("Selecione quem deseja cadastrar.");
		lblSelecioneQuemDeseja.setBounds(189, 12, 272, 15);
		contentPane_Paciente.add(lblSelecioneQuemDeseja);
		
		JLabel label = new JLabel("");
		label.setBounds(60, 297, 70, 15);
		contentPane_Paciente.add(label);
		
		final JLabel lblNome = new JLabel("Nome:");
		lblNome.setBounds(12, 70, 70, 15);
		contentPane_Paciente.add(lblNome);
		
		textField_Nome = new JTextField();
		textField_Nome.setBounds(88, 68, 183, 19);
		contentPane_Paciente.add(textField_Nome);
		textField_Nome.setColumns(10);
		
		final JLabel lblIdade = new JLabel("Idade:");
		lblIdade.setBounds(12, 105, 70, 15);
		contentPane_Paciente.add(lblIdade);
		
		textField_Idade = new JTextField();
		textField_Idade.setBounds(88, 103, 185, 19);
		contentPane_Paciente.add(textField_Idade);
		textField_Idade.setColumns(10);
		
		final JLabel lblPeso = new JLabel("Peso:");
		lblPeso.setBounds(12, 132, 70, 15);
		contentPane_Paciente.add(lblPeso);
		
		textField_Peso = new JTextField();
		textField_Peso.setBounds(90, 130, 175, 19);
		contentPane_Paciente.add(textField_Peso);
		textField_Peso.setColumns(10);
		

		final JLabel lblAltura = new JLabel("Altura:");
		lblAltura.setBounds(12, 159, 70, 15);
		contentPane_Paciente.add(lblAltura);
		
		textField_Altura = new JTextField();
		textField_Altura.setBounds(90, 157, 175, 19);
		contentPane_Paciente.add(textField_Altura);
		textField_Altura.setColumns(10);
		
		final JLabel lblNumeroC = new JLabel("Numero Consulta:");
		lblNumeroC.setBounds(12, 189, 142, 15);
		contentPane_Paciente.add(lblNumeroC);
		
		textField_NumeroC = new JTextField();
		textField_NumeroC.setBounds(151, 187, 114, 19);
		contentPane_Paciente.add(textField_NumeroC);
		textField_NumeroC.setColumns(10);
		
		
		final JLabel lblMedico_1 = new JLabel("Médico: ");
		lblMedico_1.setBounds(12, 216, 70, 15);
		contentPane_Paciente.add(lblMedico_1);
		
		textField_Medico = new JTextField();
		textField_Medico.setBounds(90, 214, 114, 19);
		contentPane_Paciente.add(textField_Medico);
		textField_Medico.setColumns(10);
		
		final JLabel lblDoenca = new JLabel("Doença:");
		lblDoenca.setBounds(12, 243, 70, 15);
		contentPane_Paciente.add(lblDoenca);
		
		textField_Doenca = new JTextField();
		textField_Doenca.setBounds(90, 241, 114, 19);
		contentPane_Paciente.add(textField_Doenca);
		textField_Doenca.setColumns(10);
		
		final JLabel lblMedicamento = new JLabel("Medicamento Indicado:");
		lblMedicamento.setBounds(10, 270, 202, 15);
		contentPane_Paciente.add(lblMedicamento);
		
		textField_Medicamento = new JTextField();
		textField_Medicamento.setBounds(189, 268, 114, 19);
		contentPane_Paciente.add(textField_Medicamento);
		textField_Medicamento.setColumns(10);
		
		final JLabel lblData = new JLabel("Data:");
		lblData.setBounds(12, 297, 70, 15);
		contentPane_Paciente.add(lblData);
		
		textField_Data = new JTextField();
		textField_Data.setBounds(90, 293, 114, 19);
		contentPane_Paciente.add(textField_Data);
		textField_Data.setColumns(10);
		
		final JLabel lblDiagnostico = new JLabel("Diagnostico:");
		lblDiagnostico.setBounds(12, 324, 129, 15);
		contentPane_Paciente.add(lblDiagnostico);
		
		textField_Diagnostico = new JTextField();
		textField_Diagnostico.setBounds(129, 324, 114, 19);
		contentPane_Paciente.add(textField_Diagnostico);
		textField_Diagnostico.setColumns(10);
		
		final JLabel lblNome_2 = new JLabel("Nome:");
		lblNome_2.setBounds(391, 70, 70, 15);
		contentPane_Paciente.add(lblNome_2);
		
		textField_Nome2 = new JTextField();
		textField_Nome2.setBounds(455, 68, 114, 19);
		contentPane_Paciente.add(textField_Nome2);
		textField_Nome2.setColumns(10);
		
		final JLabel lblCRM = new JLabel("CRM:");
		lblCRM.setBounds(391, 105, 70, 15);
		contentPane_Paciente.add(lblCRM);
		
		textField_CRM = new JTextField();
		textField_CRM.setBounds(455, 103, 114, 19);
		contentPane_Paciente.add(textField_CRM);
		textField_CRM.setColumns(10);
		
		final JLabel lblAnos = new JLabel("Anos de Experiencia:");
		lblAnos.setBounds(374, 146, 152, 15);
		contentPane_Paciente.add(lblAnos);
		
		textField_Anos = new JTextField();
		textField_Anos.setBounds(527, 144, 114, 19);
		contentPane_Paciente.add(textField_Anos);
		textField_Anos.setColumns(10);
		
		JButton btnCadastrarPaciente = new JButton("Cadastrar Paciente");
		btnCadastrarPaciente.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Paciente umPaciente = new Paciente(textField_Nome.getText());
				umCadastro.adicionarPaciente(umPaciente);
				limparCampos();
				JOptionPane.showMessageDialog(rootPane, textField_Nome.getText() + " foi adicionado com Sucesso ", "Sucesso", WIDTH, null); 
			}

			private void limparCampos() {
				textField_Nome.setText("");
				textField_Altura.setText("");
				textField_Idade.setText("");
				textField_Peso.setText("");
				textField_Peso.setText("");
				textField_Peso.setText("");
				textField_NumeroC.setText("");
				textField_Idade.setText("");
				textField_Peso.setText("");
				textField_Peso.setText("");
				textField_Peso.setText("");
				textField_Diagnostico.setText("");
				textField_Data.setText("");
				textField_Medico.setText("");
				textField_Doenca.setText("");
				
							
			}
		});
		btnCadastrarPaciente.setBounds(12, 357, 200, 30);
		contentPane_Paciente.add(btnCadastrarPaciente);
		
		JButton btnPesquisarPaciente = new JButton("Pesquisar Paciente");
		btnPesquisarPaciente.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
		
			}
		});
		btnPesquisarPaciente.setBounds(12, 399, 202, 30);
		contentPane_Paciente.add(btnPesquisarPaciente);
		
		
		JRadioButton rdbtnPaciente = new JRadioButton("Paciente");
		rdbtnPaciente.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lblNome.setEnabled(true);
				textField_Nome.setEnabled(true);
				lblAltura.setEnabled(true);
				textField_Altura.setEnabled(true);
				lblIdade.setEnabled(true);
				textField_Idade.setEnabled(true);
				lblPeso.setEnabled(true);
				textField_Peso.setEnabled(true);
				lblAltura.setEnabled(true);
				textField_Peso.setEnabled(true);
				lblPeso.setEnabled(true);
				textField_Peso.setEnabled(true);
				lblNumeroC.setEnabled(true);
				textField_NumeroC.setEnabled(true);
				lblIdade.setEnabled(true);
				textField_Idade.setEnabled(true);
				lblPeso.setEnabled(true);
				textField_Peso.setEnabled(true);
				lblAltura.setEnabled(true);
				textField_Peso.setEnabled(true);
				lblPeso.setEnabled(true);
				textField_Peso.setEnabled(true);
				lblDiagnostico.setEnabled(true);
				textField_Diagnostico.setEnabled(true);
				lblData.setEnabled(true);
				textField_Data.setEnabled(true);
				lblMedico_1.setEnabled(true);
				textField_Medico.setEnabled(true);
				lblDoenca.setEnabled(true);
				textField_Doenca.setEnabled(true);
				lblMedicamento.setEnabled(true);
				textField_Medicamento.setEnabled(true);
				lblNome_2.setEnabled(false);
				textField_Nome2.setEnabled(false);
				lblCRM.setEnabled(false);
				textField_CRM.setEnabled(false);
				lblAnos.setEnabled(false);
				textField_Anos.setEnabled(false);
			
			}
		});
		rdbtnPaciente.setBounds(94, 35, 149, 23);
		contentPane_Paciente.add(rdbtnPaciente);
		
		JRadioButton rdbtnMdico = new JRadioButton("Médico");
		rdbtnMdico.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lblNome_2.setEnabled(true);
				textField_Nome2.setEnabled(true);
				lblCRM.setEnabled(true);
				textField_CRM.setEnabled(true);
				lblAnos.setEnabled(true);
				textField_Anos.setEnabled(false);
				lblNome.setEnabled(false);
				textField_Nome.setEnabled(false);
				lblIdade.setEnabled(false);
				textField_Idade.setEnabled(false);
				lblPeso.setEnabled(false);
				textField_Peso.setEnabled(false);
				lblAltura.setEnabled(false);
				textField_Peso.setEnabled(false);
				lblPeso.setEnabled(false);
				textField_Peso.setEnabled(false);
				lblNumeroC.setEnabled(false);
				textField_NumeroC.setEnabled(false);
				lblIdade.setEnabled(false);
				textField_Idade.setEnabled(false);
				lblPeso.setEnabled(false);
				textField_Peso.setEnabled(false);
				lblAltura.setEnabled(false);
				textField_Peso.setEnabled(false);
				lblPeso.setEnabled(false);
				textField_Peso.setEnabled(false);
				lblDiagnostico.setEnabled(false);
				textField_Diagnostico.setEnabled(false);
				lblData.setEnabled(false);
				textField_Data.setEnabled(false);
				lblMedico_1.setEnabled(false);
				textField_Medico.setEnabled(false);
				lblDoenca.setEnabled(false);
				textField_Doenca.setEnabled(false);
				lblMedicamento.setEnabled(false);
				textField_Medicamento.setEnabled(false);
			
				}
		});
		rdbtnMdico.setBounds(420, 35, 149, 23);
		contentPane_Paciente.add(rdbtnMdico);
		
		JButton btnRemover = new JButton("Remover Paciente");
		btnRemover.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				umCadastro.removerPaciente(umCadastro.pesquisarPaciente(textField_Nome.getText()));
				JOptionPane.showMessageDialog(rootPane, textField_Nome.getText() + " foi removido com Sucesso ", "Sucesso", WIDTH, null);
			}
		});
		btnRemover.setBounds(12, 441, 202, 30);
		contentPane_Paciente.add(btnRemover);
		
		JButton btnCadastroMdico = new JButton("Cadastrar Médico");
		btnCadastroMdico.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Medico umMedico = new Medico(textField_Nome.getText());
				umCadastro.adicionarMedico(umMedico);
				limparCampos();
				JOptionPane.showMessageDialog(rootPane, textField_Nome.getText() + " foi adicionado com Sucesso ", "Sucesso", WIDTH, null); 
			}

			private void limparCampos() {
				
				textField_Nome2.setText("");
				textField_CRM.setText("");
				textField_Anos.setText("");
			}
			
		});
		btnCadastroMdico.setBounds(455, 208, 186, 30);
		contentPane_Paciente.add(btnCadastroMdico);
		
		JButton btnProcurarPorMdico = new JButton("Procurar por Médico");
		btnProcurarPorMdico.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		btnProcurarPorMdico.setBounds(455, 265, 186, 25);
		contentPane_Paciente.add(btnProcurarPorMdico);
		
		JButton btnRemoverMdico = new JButton("Remover Médico");
		btnRemoverMdico.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				umCadastro.removerMedico(umCadastro.pesquisarMedico(textField_Nome.getText()));
				JOptionPane.showMessageDialog(rootPane, textField_Nome.getText() + " foi removido com Sucesso ", "Sucesso", WIDTH, null);
			}
		});
		btnRemoverMdico.setBounds(455, 319, 186, 25);
		contentPane_Paciente.add(btnRemoverMdico);
		
		JList list = new JList();
		list.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				
			}
		});
		list.addAncestorListener(new AncestorListener() {
			public void ancestorAdded(AncestorEvent arg0) {
			}
			public void ancestorMoved(AncestorEvent arg0) {
			}
			public void ancestorRemoved(AncestorEvent arg0) {
			}
		});
		list.setBounds(226, 342, 165, 134);
		contentPane_Paciente.add(list);
		
	
	}
}
