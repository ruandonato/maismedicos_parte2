package controller;

import model.*;

import java.util.ArrayList;

import javax.swing.JOptionPane;
public class CadastroConsulta {

	ArrayList <Paciente> listaPacientes;
	ArrayList <Medico> listaMedicos;
	ArrayList <ConsultaMedica> listaConsulta;
	
	public CadastroConsulta(){
		listaPacientes = new ArrayList<Paciente>();
		listaMedicos = new ArrayList<Medico>();
		
	}

	
	public void adicionarPaciente(Paciente umPaciente){
		listaPacientes.add(umPaciente);
	}
	
	public void adicionarMedico(Medico umMedico){
		listaMedicos.add(umMedico);
	}
	
	public void removerPaciente(Paciente umPaciente){
		if(umPaciente == null){
			JOptionPane.showMessageDialog(null, "Não encontrado");
		}
		else{
		listaPacientes.remove(umPaciente);
		}
	}

	public void removerMedico(Medico umMedico){
		if(umMedico == null){
			JOptionPane.showMessageDialog(null, "Não encontrado");
		}
		else{
		listaMedicos.remove(umMedico);
		
		}
	}
	
	public Paciente pesquisarPaciente(String umNome){
		for(Paciente umPaciente : listaPacientes){
			if(umPaciente.getNome().equalsIgnoreCase(umNome))
			return umPaciente;
		}
		return null;
	}
	
	
	public Medico pesquisarMedico(String umNome){
		for(Medico umMedico : listaMedicos){
			if(umMedico.getNome().equalsIgnoreCase(umNome))
			return umMedico;
		}
		return null;
	}

	
	public String exibirPacientes(){
		String saida= "Pacientes:";
		if(listaPacientes.size()>0){
		for(Paciente umPaciente : listaPacientes){
			saida = (saida + "\n" + umPaciente.getNome());
		} 
		}
		return saida; 
	}	
	
	public String exibirMedicos(){
		String saida= "Medicos:";
		if(listaMedicos.size()>0){
		for(Medico umMedico : listaMedicos){
			saida = (saida + "\n" + umMedico.getNome());
		} 
		}
		return saida; 
	}
	

	public String caractPaciente(Paciente umPaciente){
		
		String Pac= ("  Paciente"+"\n"+ "Nome: " + umPaciente.getNome() + "\n" + "Idade: " + umPaciente.getIdade()+ "\n" +"Peso: " + umPaciente.getPeso() + "\n");
		return Pac;
	}
	
	public String caractMedico(Paciente umMedico){
		
		String Med= ("  Paciente"+"\n"+ "Nome: " + umMedico.getNome() + "\n" + "Idade: " + umMedico.getIdade()+ "\n" +"Peso: " + umMedico.getPeso() + "\n");
		return Med;
	}
	
	public String caractConsulta(ConsultaMedica umConsulta){
		
		String Con= ("  Consulta"+"\n"+ "Numero: " + umConsulta.getNumero());
		return Con;
	}
	
}